$(window).load(function() {
	var searchblock = $('.search-block');
	var searchbtn = $('.btn-search');
	var searchfield = $('.mh-input');
	var searchfieldWidth = searchblock.outerWidth() - searchbtn.outerWidth() - 10;
	searchfield.css('width', searchfieldWidth);
});